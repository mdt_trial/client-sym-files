[RD.CRI.CA--CRI Single Premium Insurance: California Method
 1/19/96 Written by Sharon Williams

 This specfile supports the CRI California Insurance Method SPGCA01.
 This specfile provides the ability to calculate the loan payment and
 insurance amount based on User Input, the Loan Record, or the 
 Loan Application Record.  User Input is similar to using Loan 
 Projections, you will be required to enter all of the information
 manually.  Using the Loan record or Loan Application record will
 allow the specfile to get all of the information from the record and
 if this specfile is run through Scripted FM, then it will update the
 record automatically.

 There are three places in this specfile that need to be customized per
 CU.  To customize, copy this file into a CU specific name.  Then
 look for the word "Customize".  

  1.  CERTIFY flag:   Set this to True if you are certifying with
                      CRI.  This will produce a calculation screen
                      based on a 360 day year.  This is the information
                      that you will want to send to CRI to verify the
                      calculation.
  2.  RUNTYPE flag:   You can set the run type if you will only be doing
                      one type of calculation.  Most likely you will 
                      want to be able to do User Input as well as update
                      one of the record types.  You will probably want 
                      to remove one of the record selections (Loan 
                      Record or Loan Application Record).
  3.  INSTYPE flag:   You will need to set up the Insurance Types and
                      their descriptions.  This information is used in
                      a help screen for User Input and is displayed
                      on the projection screen.

 The CRI Calculation is based on an Interest Type of 1 (360 Day Year).
 The loan payment is then recalculated on an Interest Type of 0 
 (365 Day Year, CU standard).  If your Credit Union uses Interest Type
 1, then remove the call to CRICARESETPAYMENT.

 The Insurance Table's Formula should be set to '0'.

 Process for Loan Record/Loan Application:

 1.  Create the Loan Record or Loan Application record.
 2.  Through Scripted FM run a customized version of RD.CRI.CA.
 3.  If you have run this on a Loan Application, before creating the 
     Loan Record from the Application, run the specfile again, being 
     sure the correct due date is in the application record.

 TECHNICAL PROCEDURES:

 CRICAINITSCREEN provides an initial screen requesting the loan 
 information.  Complete INSTTYPE and INSTTYPEDESC for the help screen 
 on Insurance Types.  This procedure may be omitted as long as the 
 following fields are completed from the loan record or other prompts.

 CRICAINITFROMLOAN at the bottom of this file provides an example of 
 the data required from a loan record.

 CRICAINITFROMAPP at the bottom of this file provides an example of 
 the data required from an application record.

  Insurance Type           INSURANCETYPE
  Payment Frequency        FREQ
  Payment Count            NUMREGPAYS
  Loan Amount              AMTREQ
  Annual Percentage Rate   APR
  Days before 1st Payment  FIRSTDAYS

 CRICAINIT reads specific parameters which contain information needed
 for the insurance calculation.  The insurance type must be valid
 upon calling.

 CRICACALCULATE performs the insurance calculation.

 RD.CRI.CA.LOANUPDATE include file updates a loan record with the
 fields obtained from CRICACALCULATE

 RD.CRI.CA.APPUPDATE include file updates a application record with the
 fields obtained from CRICACALCULATE

 CRICATOTALS displays the results of the calculation on a screen
 similiar to the loan projections screen.  This may be omitted if
 the specfile is being used to update a loan.  To get the results of
 the calculation, place the following into appropriate fields:
 
 Life Insurance        MONEY((DECLPREM+5E-3)*100)
 Disability Insurance  MONEY((DISPREM+5E-3)*100)  
 Payment Amount        MONEY(PAYMENT*100)+0.0000001

 Release      : 2007.02
 Modified By  : Patrick Greaney
 Modified On  : 09/17/2007 
 Modifications: Added code to projections to include enhancements 
                for Member Birthdate Insurance calcs project.
]

TARGET=ACCOUNT

DEFINE
 #INCLUDE "RD.ENTER.DEF"
 #INCLUDE "RD.GETDATA.DEF"
 #INCLUDE "RD.CRI.CA.DEF"
 #INCLUDE "RD.CRI.SCREEN.DEF"
 RUNTYPE=NUMBER   [0=Screen Input, 1=Loan Input, 2=Loan App Input]
 CERTIFY=NUMBER   [True if sending loan calculation info to CRI for
                   certification]
END

SETUP
 LOANDATE=SYSTEMDATE
[Set CERTIFY to TRUE if sending loan calculation information to CRI 
 for certification]
 CERTIFY=FALSE
[Customize the following to determine how to run this specfile]
 RUNTYPE=-1
 IF RUNTYPE<0 THEN
  DO
   PRINT "(0) Enter Data on Screen"
   NEWLINE
   PRINT "(1) Get Data from Loan"
   NEWLINE
   PRINT "(2) Get Data from Loan Application"
   NEWLINE
   NEWLINE
   RUNTYPE=ENTERNUMBER("Enter Run Type:",0)
  END
 IF RUNTYPE<0 AND RUNTYPE>2 THEN
  DO
   PRINT "You have chosen an incorrect Run Type"
   NEWLINE
   TERMINATE
  END
[Customize this section for your Insurance Types - up to 6 types]
[
 INSTYPE(1)=13
 INSTYPEDESC(1)="Single Life/Disability"
 INSTYPE(2)=14
 INSTYPEDESC(2)="Joint Life/Disability"
 INSTYPE(3)=15
 INSTYPEDESC(3)="Single Life"
 INSTYPE(4)=16
 INSTYPEDESC(4)="Joint Life"
 INSTYPE(5)=17
 INSTYPEDESC(5)="Disability"
]

[Call the following procedure to get data from a prompt screen]
 IF RUNTYPE=0 THEN
  CALL CRICAINITSCREEN  
[Call the following procedures to get data from Loan ]
 ELSE IF RUNTYPE=1 THEN
  DO
   CALL CRICAGETLOAN
   CALL CRICAINITFROMLOAN
  END
[Call the following procedures to get data from Loan Application]
 ELSE IF RUNTYPE=2 THEN
  DO
   CALL CRICAGETAPP   
   CALL CRICAINITFROMAPP
  END
 CALL CRICAINIT
END

PRINT TITLE = "CRI CA Insurance Calculation"
 CALL CRICACALCULATE 

[Display Calculation per CRI standards]
 IF CERTIFY=TRUE THEN
  DO 
   NEWPAGE
   PRINT "CRI Calculation with Monthly 360 Day Interest"
   NEWLINE
   CALL CRICATOTALS
  END

[To update insurance fields in a Loan Record.]
 IF RUNTYPE=1 THEN
  DO
   #INCLUDE "RD.CRI.CA.LOANUPDATE"
  END
[To update insurance fields in a Loan Application Record.]
 ELSE IF RUNTYPE=2 THEN
  DO
   #INCLUDE "RD.CRI.CA.APPUPDATE"
  END

[Call the following procedure to reset the loan payment based on an 
 Interest rate type of '0' (CU standard) instead of '1' (CRI standard).]
 CALL CRICARESETPAYMENT
 NEWPAGE
 PRINT "CU Calculation with Daily 365 Day Interest"
 NEWLINE
 CALL CRICATOTALS

[Update Loan record or Loan Application Record with new payment amount]
 IF LCDATASOURCE=1 THEN
  DO
   FMPERFORM REVISE LOAN FMID (1,0,FMERROR)
    DO
     SET PAYMENT TO MONEY(PAYMENT*100)+0.0000001
    END
  END
 ELSE IF LCDATASOURCE=2 THEN
  DO
   FMPERFORM REVISE LOANAPP FMID (1,0,FMERROR)
    DO
     SET PAYMENT TO MONEY(PAYMENT*100)+0.0000001
    END
  END
END

#INCLUDE "RD.CRI.SCREEN.PRO"
#INCLUDE "RD.CRI.CA.PRO"


PROCEDURE CRICAINITFROMLOAN

[This procedure may be called to retrieve the data from a loan record.
 You must already be on the appropriate loan record.]

 LCDATASOURCE=1
 INSURANCETYPE=LOAN:INSURANCETYPE
 IF INSURANCETYPE=0 THEN
  DO
   PRINT "No Insurance Has Been Chosen"
   NEWLINE
   TERMINATE
  END
 CALL CRICAGETINSURANCETYPE
 IF T<>1 THEN
  TERMINATE

 IF LOAN:PAYMENTFREQUENCY=4 THEN
  FREQ=0
 ELSE IF LOAN:PAYMENTFREQUENCY=5 THEN
  DO
   FREQ=1
   DUEDAY1=LOAN:DUEDAY1
   DUEDAY2=LOAN:DUEDAY2
  END
 ELSE IF LOAN:PAYMENTFREQUENCY=9 THEN
  FREQ=2
 ELSE IF LOAN:PAYMENTFREQUENCY=8 THEN
  FREQ=3
 ELSE
  DO
   PRINT "Invalid Payment Frequency, Only 4, 5, 8, and 9 are Supported"
   NEWLINE
   TERMINATE
  END
 NUMREGPAYS=LOAN:PAYMENTCOUNT
 AMTREQ=FLOAT(LOAN:LRLNBALANCE/100)
 APR=LOAN:INTERESTRATE
 IF LOAN:OPENDATE<>'--/--/--' THEN
  LOANDATE=LOAN:OPENDATE
 FIRSTDAYS=LOAN:DUEDATE-LOANDATE
 [** added for Member Birthdate insurance calcs project **]
 FIRSTINSBIRTHDATE=LOAN:FIRSTINSBIRTHDATE
 SECONDINSBIRTHDATE=LOAN:SECONDINSBIRTHDATE
END

PROCEDURE CRICAINITFROMAPP

[This procedure may be called to retrieve the data from an app record.
 You must already be on the appropriate app record.]

 LCDATASOURCE=2
 INSURANCETYPE=LOANAPP:INSURANCETYPE
 IF INSURANCETYPE=0 THEN
  DO
   PRINT "No Insurance Has Been Chosen"
   NEWLINE
   TERMINATE
  END
 CALL CRICAGETINSURANCETYPE
 IF T<>1 THEN
  TERMINATE
 IF LOANAPP:PAYMENTFREQUENCY=4 THEN
  FREQ=0
 ELSE IF LOANAPP:PAYMENTFREQUENCY=5 THEN
  DO
   FREQ=1
   DUEDAY1=LOANAPP:DUEDAY1
   DUEDAY2=LOANAPP:DUEDAY2
  END
 ELSE IF LOANAPP:PAYMENTFREQUENCY=9 THEN
  FREQ=2
 ELSE IF LOANAPP:PAYMENTFREQUENCY=8 THEN
  FREQ=3
 ELSE
  DO
   PRINT "INVALID PAYMENT FREQUENCY, ONLY 4, 5, 8, AND 9 ARE SUPPORTED"
   NEWLINE
   TERMINATE
  END

 MONTHS=LOANAPP:TERMMONTHS
 CALL CRICAGETPAYMENTCOUNT

 AMTREQ=FLOAT(LOANAPP:LOANAMOUNT/100)
 APR=LOANAPP:INTERESTRATE
 IF LOANAPP:BORROWDATE<>'--/--/--' THEN
  LOANDATE=LOANAPP:BORROWDATE
 FIRSTDAYS=LOANAPP:FIRSTDUEDATE-LOANDATE
 [** added for Member Birthdate insurance calcs project **]
 FIRSTINSBIRTHDATE=LOANAPP:FIRSTINSBIRTHDATE
 SECONDINSBIRTHDATE=LOANAPP:SECONDINSBIRTHDATE
END
 
PROCEDURE CRICAGETPAYMENTCOUNT

 IF FREQ=0 THEN  [MONTHLY]
  PPYR=12.0000000
 ELSE IF FREQ=1 THEN  [SEMIMONTLY]
  PPYR=24.0000000
 ELSE IF FREQ=2 THEN  [WEEKLY]
  PPYR=52.0  [LEFT OUT DECIMAL BECAUSE SYMITAR CODE DOES]
 ELSE IF FREQ=3 THEN  [BIWEEKLY]
  PPYR=26.071429 [365/14]
 PPMON=PPYR/12
 NUMREGPAYS=((MONTHS*PPMON)+5.0E-1)
END
