[RD.COLLECTION.ADDINFO.EX1

-Abstract:
 An example of a specfile used to populate the optional "Additional
 Information" column that can show up in a Collection Queue list.  
 This particular example shows total of any pending share deposits
 to the account (any share) in the ACH warehouse.

-History:
 05.027      02/04/2005 Carl Barlow III  Original Author
 10.044      02/11/2010 Changed @CPADDITIONALINFO to @CPADDITIONALINFO1
                        Initialized @CPADDITIONALINFO2 and ...3

-Details: Search the ACHITEM records in the warehouse that have a
 status of PENDING (0) that are for the current account.  For any
 that have a TRANSACTIONCODE of 22, 26, 32, 36, or 52 (different 
 kinds of deposits), add up the values in the AMOUNT fields.  If the 
 total is greater than $0.00, format a message with the total and put 
 the message in @CPADDITIONALINFO1 (up to 40 characters) so that it 
 will appear in the "Additional Info" column.

-Related Specfiles/Tips for Running:

 Rename to specfile to COLLECTION.ADDINFO (with no "RD." prefix), 
 customize as necessary, and install for demand use.

 From Business Design:
 Sample Queue Additional Field PowerOn Specfile
 We will create an example specfile, which will get current payment 
 information from Note records for the Additional Information field. 
 It will be formatted as one of the following:

   Deposit of $xx.xx on MM/DD/YY
   Payment of $xx.xx on MM/DD/YY
]

COLLECTION

TARGET=ACCOUNT

DEFINE
 DEPAMT=MONEY
 TMPCHR=CHARACTER

 ACHTRANDEMANDDEPOSITRETURN  =21
 ACHTRANDEMANDDEPOSIT        =22
 ACHTRANDEMANDDEPOSITPRENOT  =23
 ACHTRANDEMANDPAYMENTRETURN  =26
 ACHTRANDEMANDPAYMENT        =27
 ACHTRANDEMANDPAYMENTPRENOT  =28
 
 ACHTRANSAVINGSDEPOSITRETURN =31
 ACHTRANSAVINGSDEPOSIT       =32
 ACHTRANSAVINGSDEPOSITPRENOT =33
 ACHTRANSAVINGSPAYMENTRETURN =36
 ACHTRANSAVINGSPAYMENT       =37
 ACHTRANSAVINGSPAYMENTPRENOT =38

 ACHTRANLOANDEPOSITRETURN    =51
 ACHTRANLOANDEPOSIT          =52
 ACHTRANLOANDEPOSITPRENOT    =53
 ACHTRANLOANREVERSAL         =55
 ACHTRANLOANREVERSALRETURN   =56

 TRUE=1
 FALSE=0
END

PRINT TITLE="Additional Coll Queue Info"
 HEADER=""

 @CPADDITIONALINFO1=""
 @CPADDITIONALINFO2=""
 @CPADDITIONALINFO3=""
 DEPAMT=$0.00
 FOR ACHITEM WITH ACCOUNT ACCOUNT:NUMBER
  DO
   IF ACHITEM:STATUS=0 THEN   [Pending]
    DO
     IF (ACHITEM:TRANSACTIONCODE=ACHTRANDEMANDDEPOSIT OR
         ACHITEM:TRANSACTIONCODE=ACHTRANDEMANDPAYMENTRETURN OR
         ACHITEM:TRANSACTIONCODE=ACHTRANSAVINGSDEPOSIT OR
         ACHITEM:TRANSACTIONCODE=ACHTRANSAVINGSPAYMENTRETURN OR
         ACHITEM:TRANSACTIONCODE=ACHTRANLOANDEPOSIT) THEN
      DO
       DEPAMT=DEPAMT+ACHITEM:AMOUNT
      END
    END
  END
 IF DEPAMT<=$0.00 THEN
  @CPADDITIONALINFO1=""
 ELSE
  DO
[                 123456789012345]
   TMPCHR=FORMAT("###,###,##9.99+",DEPAMT)
   WHILE (SEGMENT(TMPCHR,1,1)="")
    DO
     TMPCHR=SEGMENT(TMPCHR,2,LENGTH(TMPCHR))
    END
 [                  1234567890123456789012312345]
   @CPADDITIONALINFO1="ACH Dep/Pmt Pending: $"+TMPCHR
  END
END

TOTAL
END
