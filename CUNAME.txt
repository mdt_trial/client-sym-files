JSTEXT="NameDataSet2 = ["
 CALL WRITEJS
 IF ANY NAME WITH (NAME:FIRST<>"") OR
    ANY SHARE WITH (ANY SHARE NAME WITH(SHARE NAME:FIRST<>"")) THEN
  DO
   JSTEXT="               {toString: function()"
   CALL WRITEJS
   JSTEXT="               {return ' '},"
   CALL WRITEJS
   JSTEXT="               txtName1:'',"
   CALL WRITEJS
   JSTEXT="               txtMiddle1:'',"
   CALL WRITEJS
   JSTEXT="               txtDOB1:'',"
   CALL WRITEJS
   JSTEXT="               txtAddress1:'',"
   CALL WRITEJS
   JSTEXT="               txtCity1:'',"
   CALL WRITEJS
   JSTEXT="               txtState1:'',"
   CALL WRITEJS
   JSTEXT="               txtZipCode1:'',"
   CALL WRITEJS
   JSTEXT="               txtCountry1:'',"
   CALL WRITEJS
   JSTEXT="               txtSSN1:'',"
   CALL WRITEJS
   JSTEXT="               },"
   CALL WRITEJS
  END

 FOR EACH NAME WITH (NAME:TYPE=2 OR NAME:TYPE=9)
  DO
   JSTEXT="               {toString: function()"
   CALL WRITEJS
   JSTEXT="               {return '"+CAPITALIZE(NAME:LAST)+"'},"
   CALL WRITEJS
   JSTEXT="               txtName1: '"+CAPITALIZE(NAME:FIRST)+"',"
   CALL WRITEJS
   JSTEXT="               txtMiddle1: '"+CAPITALIZE(NAME:MIDDLE)+"',"
   CALL WRITEJS
   JSTEXT="               txtDOB1: '"+FORMAT("99/99/9999",NAME:BIRTHDATE)+"',"
   CALL WRITEJS
   JSTEXT="               txtAddress1: '"+CAPITALIZE(NAME:STREET) +" "+ NAME:EXTRAADDRESS+"',"
   CALL WRITEJS
   JSTEXT="               txtCity1: '"+CAPITALIZE(NAME:CITY)+"',"
   CALL WRITEJS
   JSTEXT="               txtState1: '"+NAME:STATE+"',"
   CALL WRITEJS
   JSTEXT="               txtZipCode1: '"+NAME:ZIPCODE+"',"
   CALL WRITEJS
   JSTEXT="               txtCountry1: '"+NAME:COUNTRY+"',"
   CALL WRITEJS
   JSTEXT="               txtSSN1: '"+NAME:SSN+"',"
   CALL WRITEJS
   JSTEXT="               },"
   CALL WRITEJS
  END

 FOR EACH SHARE
  DO FOR EACH SHARE NAME WITH (SHARE NAME:TYPE=2 OR NAME:TYPE=9)
   DO
    JSTEXT="               {toString: function()"
    CALL WRITEJS
    JSTEXT="               {return '"+CAPITALIZE(SHARE NAME:LAST)+"'},"
    CALL WRITEJS
    JSTEXT="               txtName1: '"+CAPITALIZE(SHARE NAME:FIRST)+"',"
    CALL WRITEJS
    JSTEXT="               txtMiddle1: '"+CAPITALIZE(SHARE NAME:MIDDLE)+"',"
    CALL WRITEJS
    JSTEXT="               txtDOB1: '"+FORMAT("99/99/9999",SHARE NAME:BIRTHDATE)+"',"
    CALL WRITEJS
    JSTEXT="               txtAddress1: '"+CAPITALIZE(SHARE NAME:STREET) +" "+ SHARE NAME:EXTRAADDRESS+"',"
    CALL WRITEJS
    JSTEXT="               txtCity1: '"+CAPITALIZE(SHARE NAME:CITY)+"',"
    CALL WRITEJS
    JSTEXT="               txtState1: '"+SHARE NAME:STATE+"',"
    CALL WRITEJS
    JSTEXT="               txtZipCode1: '"+SHARE NAME:ZIPCODE+"',"
    CALL WRITEJS
    JSTEXT="               txtCountry1: '"+SHARE NAME:COUNTRY+"',"
    CALL WRITEJS
    JSTEXT="               txtSSN1: '"+SHARE NAME:SSN+"',"
    CALL WRITEJS
    JSTEXT="               },"
    CALL WRITEJS
   END
  END
 JSTEXT="                ];"
 CALL WRITEJS

 JSTEXT="var DropDown = this.getField('ddName2');"
 CALL WRITEJS
 JSTEXT="DropDown.clearItems();"
 CALL WRITEJS
 JSTEXT="DropDown.setItems(NameDataSet2);"
 CALL WRITEJS
